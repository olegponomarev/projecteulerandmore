package edu.mipt.ponomarev.euler;

/**
 * Created by Oleg on 28.01.2016.
 */
public class T36_DoubleBasePalindroms {
    public static boolean isPalindromic (String number){
        for (int i = 0; i < number.length() / 2; i++){
            if (number.charAt(i) != number.charAt(number.length() - 1 - i)){
                return false;
            }
        }
        return true;
    }

    public static String decimalToBinary (int number){
        long binary = 0;
        int n = -1;
        String s = "";
        int i = 1;
        while (i * 2 <= number){
            i = i * 2;
            n++;
        }
        for (int k = i; k > 1; k = k / 2){
            s += number / k;
            number = number - k * (number / k);
        }
        if (number == 0){
            s += "0";
        } else {
            s += "1";
        }
        return s;
    }

    public static void main(String[] args){
        System.out.println(decimalToBinary(585));
        int sum = 0;
        for (int i = 1; i < 1000000; i++){
            if (isPalindromic("" + i) && isPalindromic(decimalToBinary(i))){
                sum += i;
            }
        }
        System.out.println(sum);
    }
}
