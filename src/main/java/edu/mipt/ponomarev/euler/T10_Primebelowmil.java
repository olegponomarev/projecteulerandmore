package edu.mipt.ponomarev.euler;

/**
 * Created by Пономарев on 13.08.2015.
 */
public class T10_Primebelowmil {
    public static void main(String[] args){
        int i=1,j,k;
        boolean p;
        long num=2L,sum=2L;
        while (num<2000000){
            num++;
            k=1;
            while (k*k<=num){
                k++;
            }
            p=true;
            for (j=2;j<k+1;j++){
                if (num % j ==0){
                    p=false;
                    break;
                }
            }
            if (p){
                i++;
                sum=sum+num;
            }
        }
        System.out.print("the sum is " + sum + " and its numerical order is " + i);
    }
}
