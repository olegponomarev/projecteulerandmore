package edu.mipt.ponomarev.euler;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Oleg on 02.04.2016.
 */
public class T54_PokerHands {
    static String path = "D:\\GoogleDisk\\IdeaProjects\\Euler#2\\src\\main\\resources\\p054_poker.txt";
    private static class Hand implements Comparable{
        private String[] cards;
        /**
         * 0 - high card, 9 - royal flush
         */
        private int combination;
        private int highCard;

        public int getCombination() {
            return combination;
        }

        public void setCombination(int combination) {
            this.combination = combination;
        }

        public int getHighCard() {
            return highCard;
        }

        public void setHighCard(int highCard) {
            this.highCard = highCard;
        }

        public void sortCards(){

        }

        public int compareTo(Object o) {
            if (o.getClass() != this.getClass()){
                throw new IllegalArgumentException("wrong comparable object");
            }
            Hand that = (Hand) o;
            if (this.getCombination() > that.getCombination()){
                return +1;
            } else if (this.getCombination() < that.getCombination()){
                return -1;
            } else if (this.getHighCard() > that.getHighCard()){
                return +1;
            } else if (this.getHighCard() < that.getHighCard()){
                return -1;
            }
            return 0;
        }
        public Hand(String s){
            cards = s.split("\\s");
            sortCards();
            /*for (String card : getCards()){
                int currentCard = 0;
                try {
                    currentCard = Integer.parseInt(s.substring(0, 1));
                } catch (NumberFormatException e){
                    char c = card.charAt(0);
                    switch (c){
                        case 'T': currentCard = 10;
                            break;
                        case 'J': currentCard = 11;
                            break;
                        case 'Q': currentCard = 12;
                            break;
                        case 'K': currentCard = 13;
                            break;
                        case 'A': currentCard = 14;
                    }
                }
                if (currentCard > getHighCard()){
                    setHighCard(currentCard);
                }
            }*/
        }
        public String[] getCards() {
            return cards;
        }

        @Override
        public String toString() {
            String s = "";
            for (String card : cards){
                s += card + " ";
            }
            return s;
        }
    }

    public static void main(String[] args){
        String s = "8C TS KC 9H 4S 7D 2S 5D 3S AC";
        String s0 = s.substring(0, s.length() /2);
        Hand hand = new Hand(s0);
        System.out.println(hand);
//        System.out.println(s1);
//        System.out.println(s2);
        int count = 0;
        try {
            BufferedReader reader = new BufferedReader(new FileReader(path));
            String line;
            while ((line = reader.readLine()) != null){
                String s1 = line.substring(0, line.length() / 2);
                String s2 = line.substring(line.length() / 2 + 1, line.length());
                Hand hand1 = new Hand(s1);
                Hand hand2 = new Hand(s2);
                if (hand1.compareTo(hand2) > 0){
                    count++;
                }
            }
        } catch (Exception e){
            e.printStackTrace();
        }
        System.out.println(count);
    }
}
