package edu.mipt.ponomarev.euler;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 * Created by Oleg Ponomarev on 25.08.2015.
 */
public class T18_PathSum1 {
    public static final String PATH = "D://Box Sync//Java resources/T18.txt";
    public static void main (String args[]) throws FileNotFoundException{
        Scanner scanner = new Scanner(new File(PATH));
        int size = 15;
        String[] s1= new String [size];
        int[][] mas=new int[size][size];
        int a=-1, i=0,j=0,sum,num=0;
        String s="";
        while (scanner.hasNextInt()) {
            mas[i][j++] = scanner.nextInt();
            if (j > i) {
                i++;
                j = 0;
            }
        }
        sum=mas[0][0];
        /*for (i=0;i<size;i++){
            for (j=0;j<size;j++){
                System.out.print(mas[i][j]+" ");
            }
            System.out.println();
        }*/
        for (i=1;i<size;i++){
            if (mas[i][num]>mas[i][num+1]){
                sum=sum+mas[i][num];
            }
            else {
                sum=sum+mas[i][++num];
            }
        }
        System.out.println(sum);
    }
}
