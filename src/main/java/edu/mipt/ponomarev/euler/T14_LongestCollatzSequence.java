package edu.mipt.ponomarev.euler;

/**
 * Created by Пономарев on 13.08.2015.
 */
public class T14_LongestCollatzSequence {
    public static void main(String[] args) {
        int i, maxi = 0;
        long num,num0, maxN = 0L;
        for (num0 = 2L; num0 < 1000000L; num0++) {
            num = num0;
            i = 1;
            do {
                //System.out.print(num + " ");
                if ((num % 2) == 0) {
                    num = num / 2;
                } else {
                    num = num * 3 + 1;
                }
                i++;
            } while (num > 1);
            //System.out.println("; Length is "+i);
            if (i > maxi) {
                maxi = i;
                maxN = num0;
            }
        }
        System.out.println("First number is " + maxN + ", length is " + maxi);
    }
}
