package edu.mipt.ponomarev.euler;

/**
 * Created by Oleg on 19.03.2016.
 */
public class T53_CombinatoricSelections {
    public static boolean coefficientExceedsMillion (int n, int k){
        int kInitial= k;
        int difference = n - k;
        long fracture = k + 1;
        k = k + 2;
        int i = 2;
        while (k <= n || i <= difference){
            if (fracture % i == 0 && i <= difference){
                fracture = fracture / i;
                if (k > n && fracture < 1000000){
                    return false;
                }
                i++;
            } else {
                if (k > n || fracture < 0) throw new ArithmeticException("k= " + kInitial + "n= " + n);
                fracture = fracture * k;
                if (i > difference && fracture > 1000000){
                    return true;
                }
                k++;
            }
        }
//        System.out.println(fracture);
        return fracture > 1000000;
    }
    public static void main(String[] args){
        System.out.println(coefficientExceedsMillion(63, 36));
        /*int sum = 0;
        for (int n = 63; n <= 100; n++){
            for (int k = 1; k < n; k++){
                if (coefficientExceedsMillion(n, k)){
                    sum++;
                }
            }
        }
        System.out.println(sum);*/
    }
}
