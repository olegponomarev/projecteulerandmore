package edu.mipt.ponomarev.euler;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Oleg Ponomarev on 25.08.2015.
 */
public class T22_NamesScores {
    public static final String PATH = "D:\\GoogleDisk\\IdeaProjects\\Euler#2\\src\\main\\resources\\T22_test.txt";
    public static void main (String args[]) {
        long count = 0L;
        List<String> names = new ArrayList<>();
        try {
            BufferedReader reader = new BufferedReader(new FileReader(PATH));
            String line;
            while ((line = reader.readLine()) != null){
                names = Arrays.asList(line.split(","));
            }
            String[] array = new String[names.size()];
            for (int i = 0; i < names.size(); i++){
                array[i] = names.get(i);
            }
            for (int i = 0; i < names.size() - 1; i++){
                for (int j = i + 1; j < names.size(); j++){
                    if (array[i].compareTo(array[j]) > 0){
                        String s = array[i];
                        array[i] = array[j];
                        array[j] = s;
                    }
                }
            }
            for (int i = 0; i < array.length; i++){
                char[] letters = array[i].toCharArray();
                int value = 0;
                for (int j = 1; j < letters.length - 1; j++){
                    value += (int)letters[j] - 64;
                }
                count += (i + 1)*value;
                System.out.println(count);
            }
            //System.out.println(array[937]);
            /*for (String s : array){
                System.out.println(s);
            }*/
        }
        catch (Exception e){e.printStackTrace();}
    }
}
