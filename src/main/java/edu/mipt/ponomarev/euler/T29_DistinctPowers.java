package edu.mipt.ponomarev.euler;

import java.util.ArrayList;
import java.util.List;

import static edu.mipt.ponomarev.euler.util.Utilities.getBthPowerOfA;

/**
 * Created by Oleg on 30.07.2016.
 */
public class T29_DistinctPowers {
    private static final int A_LEFT = 3;
    private static final int A_RIGHT = 141;
    private static final int B_LEFT = 3;
    private static final int B_RIGHT= 100;
    public static void main(String[] args) {
        System.out.println(getNumberOfDistinctTerms(A_RIGHT , B_RIGHT));
    }

    private static int getNumberOfDistinctTerms(int a, int b) {
        int count = 0;
        List<String> results = new ArrayList<>(a * b);
        for (int i = A_LEFT; i <= a; i++) {
            for (int j = B_LEFT; j <= b; j++) {
                String term = getBthPowerOfA("" + i, "" + j);
                if (!results.contains(term)) {
                    results.add(term);
                    count++;
                }
            }
        }
        return count;
    }
}
