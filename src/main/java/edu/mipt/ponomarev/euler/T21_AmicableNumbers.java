package edu.mipt.ponomarev.euler;

/**
 * Created by Пономарев on 25.08.2015.
 */
public class T21_AmicableNumbers {
    public static int getSum (int x){
        int sum=0;
        for (int i=1;i<=(x/2);i++){
            if ((x%i)==0){
                sum=sum+i;
            }
        }
        return sum;
    }
    public static void main (String [] args){
        int a,b,sum=0;
        for (int i=2;i<10000;i++){
            a=getSum(i);
            b=getSum(a);
            if ((b==i)&&(a!=b)){
                System.out.println(i+" "+a);
                sum=sum+i;
            }
        }
        System.out.println(sum);
    }
}
