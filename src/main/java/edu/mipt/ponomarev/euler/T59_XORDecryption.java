package edu.mipt.ponomarev.euler;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

/**
 * A modern encryption method is to take a text file, convert the bytes to ASCII, then XOR each byte with a given value,
 * taken from a secret key. The advantage with the XOR function is that using the same encryption key on the cipher text,
 * restores the plain text; for example, 65 XOR 42 = 107, then 107 XOR 42 = 65.
 * <p>
 * For unbreakable encryption, the key is the same length as the plain text message, and the key is made up of random
 * bytes. The user would keep the encrypted message and the encryption key in different locations, and without both
 * "halves", it is impossible to decrypt the message.
 * <p>
 * Unfortunately, this method is impractical for most users, so the modified method is to use a password as a key.
 * If the password is shorter than the message, which is likely, the key is repeated cyclically throughout the message.
 * The balance for this method is using a sufficiently long password key for security, but short enough to be memorable.
 * <p>
 * Your task has been made easy, as the encryption key consists of three lower case characters. Using cipher.txt,
 * a file containing the encrypted ASCII codes, and the knowledge that the plain text must contain common English words,
 * decrypt the message and find the sum of the ASCII values in the original text.
 *
 * @version 1.0
 * @Author Oleg Ponomarev
 * @since 30.12.2016
 */
public class T59_XORDecryption {
    private static final String PATH_IN = "D:\\GoogleDisk\\IdeaProjects\\Euler#2\\src\\main\\resources\\p059_cipher.txt";
    private static final Path INPUT = Paths.get( PATH_IN );

    public static void main ( String[] args ) throws IOException {
        List < String > list = new ArrayList <>();
        Files.lines( INPUT ).forEach( list::add );

        String[] bytes = list.get( 0 ).split( "," );
        byte[] message = new byte[bytes.length];
        int index = 0;
        for ( String b : bytes ) {
            message[index++] = Byte.parseByte( b );
        }

        List < String > decryptedMessages = new ArrayList <>();
        for ( char first = 'a'; first <= 'z'; first++ ) {
            for ( char second = 'a'; second <= 'z'; second++ ) {
                for ( char third = 'a'; third <= 'z'; third++ ) {
                    String decrypted = decryptMessage( message, String.valueOf( first ) + second + third );
                    decryptedMessages.add( decrypted );
                }
            }
        }

        String result = null;
        for ( String s : decryptedMessages ) {
            if ( s.contains( "world" ) ) { //just a random word
                result = s;
                System.out.println( s );
            }
        }

        long sum = 0;
        for (byte b : result.getBytes()) {
            sum += b;
        }
        System.out.println(sum);
    }

    private static String decryptMessage ( byte[] message, String key ) {
        StringBuilder result = new StringBuilder();
        while ( key.length() < message.length ) {
            key += key;
        }
        key = key.substring( 0, message.length );
        int index = 0;
        for ( byte b : message ) {
            result.append( ( char ) ( b ^ key.charAt( index++ ) ) );
        }
        return result.toString();
    }
}
