package edu.mipt.ponomarev.euler;


import static edu.mipt.ponomarev.euler.util.Utilities.getProduct;
import static edu.mipt.ponomarev.euler.util.Utilities.sumOfLines;

/**
 * @version 1.0
 * @Author Oleg Ponomarev
 * @since 20.11.2016
 */
public class T57_SquareRootConvergents {
    private String[][] fractions;

    public T57_SquareRootConvergents ( int number ) {
        fractions = new String[number][];
    }

    public static void main ( String[] args ) {
        T57_SquareRootConvergents convergents = new T57_SquareRootConvergents( 1001 );
        System.out.println(convergents.fractionToString( convergents.squareRoot( 0 ) ));
        System.out.println(convergents.fractionToString( convergents.squareRoot( 1 ) ));
        System.out.println(convergents.fractionToString( convergents.squareRoot( 2 ) ));
        System.out.println(convergents.fractionToString( convergents.squareRoot( 3 ) ));
        System.out.println(convergents.fractionToString( convergents.squareRoot( 8 ) ));
        System.out.println(convergents.fractionToString( convergents.squareRoot( 1000 ) ));
        int count = 0;
        for (String[] fraction : convergents.fractions) {
            if (fraction[0].length() > fraction[1].length()) {
                count++;
            }
        }
        System.out.println(count);
    }

    public String[] squareRoot ( int iterations ) {
        if (fractions[iterations] != null) {
            return fractions[iterations];
        }
        String[] fraction = new String[2];
        fraction[0] = "1";
        fraction[1] = "1";
        if (iterations == 0) {
            fractions[iterations] = fraction;
            return fraction;
        }
        for (int i = 0; i < iterations; i++) {
            fraction = sumOfFractions( new String[]{"1", "1"},
                    turn( sumOfFractions( new String[]{"1", "1"}, squareRoot( iterations - 1 ) ) ) );
        }
        simplify( fraction );
        fractions[iterations] = fraction;
        return fraction;
    }

    /**
     * I thought that I have to simplify each fraction, but it turned out to be unnecessary
     */
    private void simplify(String[] fraction) {

    }

    private String[] turn(String[] fraction) {
        String swap = fraction[0];
        fraction[0] = fraction[1];
        fraction[1] = swap;
        return fraction;
    }

    private String[] sumOfFractions (String[] a, String[] b) {
        if (a.length != b.length || a.length != 2) {
            throw new IllegalArgumentException(  );
        }
        String[] sum = new String[2];
        if (a[1].equals( b[1] ) ) {
            sum[0] = sumOfLines( a[0], b[0] );
            sum[1] = a[1];
        } else {
            sum[0] = sumOfLines( getProduct( a[0], b[1] ), getProduct( b[0], a[1] ) );
            sum[1] = getProduct( a[1], b[1] );
        }
        return sum;
    }

    public String fractionToString ( String[] fraction ) {
        return fraction[0] + "/" + fraction[1];
    }
}
