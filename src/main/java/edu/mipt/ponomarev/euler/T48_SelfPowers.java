package edu.mipt.ponomarev.euler;

/**
 * Created by Oleg on 20.01.2016.
 */
public class T48_SelfPowers {
//    finds only 10 last digits
    public static long selfPower(int number){
        long result = 1;
        for (int i = 0; i < number; i++){
            result = result * number;
            if (result > 10000000000L){
                result = result % 10000000000L;
            }
        }
        return result;
    }

    public static long lastTenDigitsOfSelfPowersSum(int number){
        long result = 0;
        for (int i = 1; i <= number; i++){
            result += selfPower(i);
            if (result > 10000000000L){
                result = result % 10000000000L;
            }
        }
        return result;
    }
    public static void main(String[] args){
        System.out.println(lastTenDigitsOfSelfPowersSum(1000));
    }
}
