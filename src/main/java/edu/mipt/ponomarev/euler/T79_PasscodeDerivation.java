package edu.mipt.ponomarev.euler;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by Oleg Ponomarev on 16.12.2015.
 */
public class T79_PasscodeDerivation
{
    public static String incrementString (String s){
        String result = "";
        int remainder = 1;
        for (int i = s.length() - 1; i >= 0; i--){
            int current = Character.getNumericValue(s.charAt(i));
            current += remainder;
            if (current > 9){
                remainder = current / 10;
                current = current % 10;
            } else {
                remainder = 0;
            }
            result = current + result;
        }
        if (remainder > 0){
            result = remainder + result;
        }
        return result;
    }
    public static void main(String[] args) throws IOException{
        BufferedReader reader = new BufferedReader(new FileReader(args[0]));
        String line;
        ArrayList<String> keys = new ArrayList<String>();
        while ((line = reader.readLine()) != null){
            keys.add(line);
        }
        reader.close();
        String passcode = "1";
        while (true){
            passcode = incrementString(passcode);
            int i;
            int count = 0;
            for (i = 0; i < keys.size(); i++){
                boolean isCounted = false;
                if (!passcode.contains("" + keys.get(i).charAt(0)) ||
                    !passcode.contains("" + keys.get(i).charAt(1)) ||
                    !passcode.contains("" + keys.get(i).charAt(2))) {
                    break;
                } else {
                    int h;
                    int j;
                    int k;
                    for (h = 0; h < passcode.length() - 2; h++){
                        if (passcode.charAt(h) == keys.get(i).charAt(0) && !isCounted){
                            for (j = h + 1; j < passcode.length() - 1; j++){
                                if (passcode.charAt(j) == keys.get(i).charAt(1) && !isCounted){
                                    for (k = j + 1; k < passcode.length(); k++){
                                        if (passcode.charAt(k) == keys.get(i).charAt(2) ){
                                            count++;
                                            isCounted = true;
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if (count == keys.size()){
                break;
            }
        }
        System.out.println(passcode);
    }
}
