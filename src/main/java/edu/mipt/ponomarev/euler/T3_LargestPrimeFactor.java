package edu.mipt.ponomarev.euler;

/**
 * Created by Oleg Ponomarev on 13.08.2015.
 */
public class T3_LargestPrimeFactor {
    public static void main(String[] args) {
        long number = 600851475143L;
        int i = 2, max = 0;
        System.out.println("privet");
        while (i <= number) {
            if (number % i == 0) {
                System.out.println(i + " " + number);
                number = number / i;
                max = i;
            }
            i++;
        }
        System.out.print("max prime factor is " + max);
    }
}
