package edu.mipt.ponomarev.euler;

import java.util.ArrayList;

/**
 * Created by Oleg Ponomarev on 23.12.2015.
 */
public class T27_QuadraticPrimes
{
    public static ArrayList<Long> primeNumbers (long limit){
        ArrayList<Long> list = new ArrayList<Long>();
        for (long i = 2L; i <= limit; i++){
            if (list.size() == 0){
                list.add(i);
            } else {
                boolean isPrime = true;
                for (Long number : list){
                    if (i % number == 0){
                        isPrime = false;
                        break;
                    }
                }
                if (isPrime){
                    list.add(i);
                }
            }
        }
        return list;
    }

    public static boolean isPrime (long number){
        if (number <= 1) return false;
        ArrayList<Long> primes = primeNumbers(number);
        return primes.contains(number);
    }

    public static void main (String[] agrs){
    //    System.out.println(primeNumbers(30));
    //    System.out.println(isPrime(30));
        int max = 0;
        int product = 0;
        for (int a = -999; a < 1000; a++){
            for (int b = 2; b < 1000; b++){
                int n = 0;
                while (isPrime(n*n + a*n + b)){
                    n++;
                }
                if (n > max){
                    max = n;
                    product = a * b;
                }
            }
        }
        System.out.println(max + " " + product);
    }
}
