package edu.mipt.ponomarev.euler;

/**
 * Created by Oleg on 26.12.2015.
 */
public class T33_DigitCancellingFraction {
    public static int[] simplify (int[] input){
        if (input.length != 2){
            throw new IllegalArgumentException("wrong fracton");
        }
        int numerator = input[0];
        int denominator = input[1];
        int i = 2;
        while (i <= numerator && i<= denominator){
            if (numerator % i == 0 && denominator % i == 0){
                numerator = numerator / i;
                denominator = denominator / i;
            } else {
                i++;
            }
        }
        int[] output = {numerator, denominator};
        return output;
    }

    public static void main (String[] args){
        /*int[] fraction = {118, 2};
        fraction = simplify(fraction);
        System.out.println (fraction[0]+"/"+fraction[1]);*/
        int count = 0;
        int[] result = {1, 1};
        for (int a = 10; a < 100; a++){
            for (int b = 10; b < 100; b++){
                if (a % 10 == 0 && b % 10 == 0){
                    continue;
                }
                if (a >= b){
                    continue;
                }
                int[] fraction = {a, b};
                int[] incorrectSimplified;
                if ((a / 10) == (b % 10)) {
                    incorrectSimplified = new int[]{a % 10, b / 10};
                } else if ((a % 10) == (b / 10)){
                    incorrectSimplified = new int[]{a / 10, b % 10};
                } else {
                    continue;
                }
                if (simplify(fraction)[0] == simplify(incorrectSimplified)[0] && simplify(fraction)[1] == simplify(incorrectSimplified)[1]){
                    count++;
                    result[0] = result[0] * simplify(fraction)[0];
                    result[1] = result[1] * simplify(fraction)[1];
                }
            }
        }
        result = simplify(result);
        System.out.println(count+" "+ result[0] + "/" + result[1]);
    }
}
