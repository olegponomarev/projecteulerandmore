package edu.mipt.ponomarev.euler;

import java.util.ArrayList;

/**
 * Created by Oleg on 22.01.2016.
 */
public class T44_PentagonNumbers {
    public static ArrayList<Integer> pentagonNumbers(int n){
        ArrayList<Integer> list = new ArrayList<Integer>();
        for (int i = 1; i <= n; i++){
            list.add(i*(3 * i - 1) / 2);
        }
        return list;
    }

    public static void main(String[] args) {
        int difference = 0;
        int n = 10;
        while (true) {
            ArrayList<Integer> list = pentagonNumbers(n);
            if (difference != 0){
                break;
            }
            for (int i = 0; i < list.size() - 1; i++) {
                for (int j = list.size() / 2; j < list.size(); j++) {
                    if (list.contains(list.get(j) - list.get(i)) && list.contains(list.get(j) + list.get(i))){
                        difference = list.get(j) - list.get(i);
                        System.out.println(difference);
                    }
                }
            }
            n = n * 2;
        }
    }
}
//