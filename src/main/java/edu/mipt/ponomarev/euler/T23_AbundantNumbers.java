package edu.mipt.ponomarev.euler;

import java.util.Collection;
import java.util.Vector;

/**
 * Created by Пономарев on 25.08.2015.
 */
public class T23_AbundantNumbers {
    public static void main (String[] args){
        Vector vector = new Vector(1,1);
        int i,j,el;
        long sum;
        for (i=2;i<28130;i++){
            sum=0L;
            for (j=1;j<=(i/2);j++){
                if ((i%j)==0){
                  sum=sum+j;
                }
            }
            if (sum>i){
                vector.addElement(i);
            }
        }
        sum=0L;
        el=0;
        for (i=1;i<28124;i++){
            boolean p=true;
            while ((Integer)vector.get(el)<i){
                el++;
            }
            for (j=0;j<=(el/2);j++){
                if (p){
                    for (int k=0;k<el;k++){
                        if ((i-(Integer)vector.get(j))==(Integer)vector.get(k)){
                            p=false;
                            break;
                        }
                    }
                }
            }
            if (p){
                sum=sum+i;
            }
        }
        System.out.println(sum);
        System.out.println (vector.toString()+" ");
        //System.out.print(vector.capacity());
    }
}
