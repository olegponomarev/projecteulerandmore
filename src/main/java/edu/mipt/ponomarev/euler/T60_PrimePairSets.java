package edu.mipt.ponomarev.euler;

import java.math.BigInteger;
import java.util.*;

public class T60_PrimePairSets {

	private long minSum5() {
		BigInteger integer = BigInteger.ONE;
		List<Long> primes = new ArrayList<>();
		Map<Long, List<Long>> map = new HashMap<>();
		while (true) {
			integer = integer.nextProbablePrime();
			final long current = integer.longValue();
			int count = 0;
			for (Long prime : primes) {
				if (isConcatPrime(prime, current)) {
					count++;
					map.computeIfAbsent(current, v -> new ArrayList<>()).add(prime);
					map.computeIfAbsent(prime, v -> new ArrayList<>()).add(current);
				}
			}
			primes.add(current);
			if (count < 5) { // don't know how to generify yet
				continue;
			}
			if (checkSolution(current, map)) {
				break;
			}
		}
		System.out.printf("%d -> %s\n", integer.longValue(), map.get(integer.longValue()));
		for (Long neighbor : map.get(integer.longValue())) {
			System.out.printf("%d -> %s\n", neighbor, map.get(neighbor));
		}
		return 0;
	}

	private boolean checkSolution(long current, Map<Long, List<Long>> map) {
		final List<Long> primes = map.get(current);
		for (int i = 0; i < primes.size(); i++) {
			for (int j = i + 1; j < primes.size(); j++) {
				for (int k = j + 1; k < primes.size(); k++) {
					for (int l = k + 1; l < primes.size(); l++) {
						Long first = primes.get(i);
						Long second = primes.get(j);
						Long third = primes.get(k);
						Long fourth = primes.get(l);
						if (map.get(first).containsAll(Arrays.asList(second, third, fourth))
								&& map.get(second).containsAll(Arrays.asList(first, third, fourth))
								&& map.get(third).containsAll(Arrays.asList(second, first, fourth))
								&& map.get(fourth).containsAll(Arrays.asList(second, third, first))) {
							long sum = first + second + third + fourth + current;
							System.out.printf("Solution:%d, %d, %d, %d, %d; sum: %d\n", current, first, second, third, fourth, sum);
							return true;
						}
					}
				}
			}

		}
		return false;
	}

	private boolean isConcatPrime(long a, long b) {
		return new BigInteger(String.format("%d%d", a, b)).isProbablePrime(10)
				&& new BigInteger(String.format("%d%d", b, a)).isProbablePrime(10);
	}

	public static void main(String[] args) {
		T60_PrimePairSets sets = new T60_PrimePairSets();
		final long sum = sets.minSum5();
	}
}