package edu.mipt.ponomarev.euler;

/**
 * Created by Oleg Ponomarev on 25.08.2015.
 */
public class T20_FactorialDigits {
    public static String getMult(String s, int i){
        int j=s.length(),mult=0,res=0;
        String s1="";
        while (j>0){
            mult=Character.getNumericValue(s.charAt(j-1))*i+res;
            if (mult>9){
                res=mult/10;
                mult=mult%10;
            }
            else {
                res=0;
            }
            s1=mult+s1;
            j--;
        }
        if (res>0){
            s1=res+s1;
        }
        return s1;
    }
    public static String getSum (String s1, String s2){
        int sum;
        String result = "";
        int residue = 0;
        if (s1.length() < s2.length()){
            String s = s1;
            s1 = s2;
            s2 = s;
        }//now s1 is the longest string
        for (int i = 0; i <= s1.length() - s2.length(); i++){
            s2 = "0" + s2;
        }
        for (int i = 0; i < s2.length(); i++){
            sum = (int)s1.charAt(s1.length() - 1 - i) + (int)s2.charAt(s2.length() - 1 - i) - 96 + residue;
            if (sum < 10){
                residue = 0;
            } else {
                residue = sum / 10;
                sum = sum % 10;
            }
            result = sum + result;
        }
        result = residue + result;
        /*int number = s1.length() - s2.length() - 1;
        if (residue > 0){
            if (number >= 0) {
                result = (residue + s1.charAt(number) - 48) + result;
            } else {
                result = residue + result;
            }
            number--;
        }*/
        /*if (s1.length() != s2.length()) {
            for (int i = number; i >= 0; i--) {
                result = s1.charAt(i) + result;
            }
        }*/

        return result;
    }

    public static void main (String[] args){
        /*String s="1",s1,s2;
        int num=100, j, res,sum;
        for (int i=2;i<num+1;i++){
            int idiv=i/10, imod=i%10;
            s1=getMult(s,imod);
            if (idiv>0){
                s2=getMult(s,idiv)+"0";
            }
            else {
                s2="";
                for (j=s1.length();j>0;j--){
                    s2=s2+"0";
                }
            }
            *//*j=s2.length()-s1.length();
            while (j-->0){
                s1="0"+s1;
            }
            res=0;
            s="";
            for (j=s1.length();j>0;j--){
                sum=Character.getNumericValue(s1.charAt(j-1))+Character.getNumericValue(s2.charAt(j-1))+res;
                if (sum>9){
                    res=sum/10;
                    sum=sum%10;
                }
                else {
                    res=0;
                }
                s=sum+s;
            }
            if (res>0){
                s=res+s;
            }*//*
            s=getSum(s1,s2);
        }
        j=s.length();
        sum=0;
        while(j-->0){
            sum=sum+Character.getNumericValue(s.charAt(j));
        }*/
        System.out.println(getSum("996", "5"));
    }
}
