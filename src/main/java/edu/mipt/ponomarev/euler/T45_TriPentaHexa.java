package edu.mipt.ponomarev.euler;

import java.util.ArrayList;

/**
 * Created by Oleg on 24.01.2016.
 */
public class T45_TriPentaHexa {
//    static ArrayList<Long> numbers = T42_CodedTriangleNumbers.triangleNumbers(from, limit);

    public static boolean isHexa(long i) {
        if ((int) Math.sqrt(i * 8 + 1) * (int) Math.sqrt(i * 8 + 1) == 1 + 8 * i &&
                ((int) Math.sqrt(i * 8 + 1) + 1) % 4 == 0) {
            return true;
        }
        return false;
    }

    //    static long limit = 5000000000L;
//    static long from = 1000000000L;
    /*public static boolean isPeta(long i) {
        if ((int) Math.sqrt(i * 24 + 1) * (int) Math.sqrt(i * 24 + 1) == 1 + 24 * i &&
                ((int) Math.sqrt(i * 24 + 1) + 1) % 6 == 0) {
            return true;
        }
        return false;
    }*/
    public static boolean isPeta(long i){
        while (true){
            for (long n = 2; ; n++){
                if (n * (3 * n - 1) == 2 * i){
                    return true;
                }
                if (n * (3 * n - 1) > 2 * i){
                    return false;
                }
            }
        }
    }


    public static void main(String[] args) {
        int count = 0;
        for (long n = 2; ; n++) {
            long i = n * (2 * n - 1);
            if (isPeta(i)) {
                System.out.println(i);
                count++;
            }
            if (count == 2){
                break;
            }
        }
    }
}
