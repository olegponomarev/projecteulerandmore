package edu.mipt.ponomarev.euler;

/**
 * Created by Oleg on 25.01.2016.
 */
public class T39_IntegerRightTriangles {
    public static int solutions(int p){
        int count = 0;
        for (int a = 1; a < p / 2; a++){
            for (int b = 1; b < p / 2; b++){
                if (a * a + b * b == (p - a - b) * (p - a - b)){
                    count++;
                }
            }
        }
        return count / 2;
    }
    public static void main(String[] args){
        int count = 0;
        int number = 0;
        for (int p = 4; p <= 1000; p++){
            if (solutions(p) > count){
                count = solutions(p);
                number = p;
            }
        }
        System.out.println("num = " + number + ", " + count + " solutions");
    }
}
