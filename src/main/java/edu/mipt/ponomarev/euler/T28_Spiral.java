package edu.mipt.ponomarev.euler;

/**
 * Created by Oleg on 01.08.2016.
 * Algorithm is the following:
 * 1) create an array of 0's
 * 2) initialize first 3 cells with 1, 2 and 3
 * 3) move to next cell
 * 4)
 */
public class T28_Spiral {
    public static final int SIZE = 1001;
    public static int[][] array = new int[SIZE][SIZE];

    public static void main(String[] args) {
        initializeArray();
        if (SIZE < 10) {
            printArray();
        }
        System.out.println(diagonalSum());
    }
    private static void printArray() {
        for (int[] row : array) {
            for (int cell : row) {
                System.out.print(cell + " ");
            }
            System.out.println();
        }
    }

    private static long diagonalSum() {
        long result = 0;
        for (int i = 0; i < SIZE; i++) {
            result += array[i][i] + array[i][SIZE - 1 - i];
        }
        result -= array[SIZE / 2][SIZE / 2];
        return result;
    }

    public static int walkDeltaX (int countTurns) {
        if (countTurns % 4 == 1 || countTurns % 4 == 3) {
            return 0;
        } else if (countTurns % 4 == 0) {
            return -1;
        } else if (countTurns % 4 == 2) {
            return 1;
        } else throw new ArithmeticException("something went wrong while counting walkDeltaX");
    }

    public static int walkDeltaY (int countTurns) {
        if (countTurns % 4 == 1) {
            return -1;
        } else if (countTurns % 4 == 0 || countTurns % 4 == 2) {
            return 0;
        } else if (countTurns % 4 == 3) {
            return 1;
        } else throw new ArithmeticException("something went wrong while counting walkDeltaX");
    }
    public static void initializeArray() {
        int i = SIZE / 2;
        int j = SIZE / 2;
        int k = 1;
//        initialize first 3 cells
        array[i][j++] = k++;
        array[i++][j] = k++;
        array[i][j] = k++;
//        now do all the stuff
        int countSteps = 0;
        int countTurns = 0;
        int stepLength = 2;
        int numberOfStepsOfCurrentLength = 0;
        while (true) {
//            do 2 steps then turn
            if (countSteps == stepLength) {
                numberOfStepsOfCurrentLength++;
                countTurns++;
                countSteps = 0;
                if (numberOfStepsOfCurrentLength == 2) {
                    numberOfStepsOfCurrentLength = 0;
                    stepLength++;
                }
            } else {
                i += walkDeltaY(countTurns);
                j += walkDeltaX(countTurns);
                countSteps++;
                if (i == SIZE || j == SIZE) {
                    break;
                }
                array[i][j] = k++;
            }
        }
    }
}
