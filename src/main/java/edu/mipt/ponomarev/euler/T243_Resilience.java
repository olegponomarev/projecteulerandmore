package edu.mipt.ponomarev.euler;

import java.util.List;

/**
 * @version 1.0
 * @Author Oleg Ponomarev
 * @since 20.11.2016
 */
public class T243_Resilience {
    private String[][] fractions;

    /**
     * Here I'll get a list of primes with Eratosthenes sieve
     * @return
     */
    private List<Long> getPrimes (long limit) {
        long[] numbers = new long[Integer.MAX_VALUE];
        return null;
    }

    /**
     * все не так. Надо считать те, которые нельзя сократить, а я считаю те, которые можно
     * хотя норм. одни + другие = знаменатель - 1
     */
    private long getSmallestDWithResilienceLessThanRatio (int[] ratio) {
        long denominator = 2;
        loop:
        while (true) {
            int count = 0;
            for (long numerator = 1; numerator < denominator; numerator++) {
                if (numerator != 1 && denominator % numerator == 0) {
                    count++;
                }
                for (long delimeter = 2; delimeter * delimeter <= numerator; delimeter++) {
                    if (numerator % delimeter == 0 && denominator % delimeter == 0) {
                        count++;
                        if (count * ratio[1] > denominator * ratio[0]) {
                            continue loop;
                        }
                    }
                }
            }
            return denominator;
        }
    }
}
