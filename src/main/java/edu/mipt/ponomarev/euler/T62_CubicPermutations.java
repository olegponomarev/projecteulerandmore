package edu.mipt.ponomarev.euler;

import java.util.ArrayList;
import java.util.List;

import static edu.mipt.ponomarev.euler.util.Utilities.arePermutations;

/**
 * The cube, 41063625 (3453), can be permuted to produce two other cubes: 56623104 (3843) and 66430125 (4053).
 * In fact, 41063625 is the smallest cube which has exactly three permutations of its digits which are also cube.
 * Find the smallest cube for which exactly five permutations of its digits are cube.
 *
 * @Author Oleg Ponomarev
 * @since 03.01.2017
 */
public class T62_CubicPermutations {
    private static List <Long> numbers = new ArrayList <>();
    private static final int NUMBER_OF_CUBIC_PERMUTATIONS = 5;

    private static void addMoreCubics () {
        int initialSize = numbers.size();
        if (initialSize == 0) {
            numbers.add(1L);
        }
        for (int index = 1; index <= initialSize; index++) {
            long currentIndex = initialSize + index;
            long currentCubic = currentIndex * currentIndex * currentIndex;
            numbers.add(currentCubic);
        }
    }

    public static void main (String[] args) {
        int index = 0;
        OUTER:
        while (true) {
            if (numbers.size() == index) {
                addMoreCubics();
            }
            Long currentNumber = numbers.get(index++);
            int count = 0;
            List <Long> resultSet = new ArrayList <>();
            for (Long number : numbers) {
                if (arePermutations(number.toString(), currentNumber.toString())) {
                    count++;
                    resultSet.add(number);
                    if (count == NUMBER_OF_CUBIC_PERMUTATIONS) {
                        System.out.println(resultSet);
                        System.out.println("Answer is " + resultSet.get(0));
                        break OUTER;
                    }
                }
            }
        }
    }
}
