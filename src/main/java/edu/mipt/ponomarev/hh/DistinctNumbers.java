package edu.mipt.ponomarev.hh;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Oleg Ponomarev on 18.09.2016.
 */
public class DistinctNumbers {
    private static final int A_LIMIT = 107;
    private static final int B_LIMIT = 106;

    public static void main(String[] args) {
        List<Double> numbers = new ArrayList<>();
//        for (int a = 2; a < 6; a++) {
//            for (int b = 2; b < 6; b++) {
//                double power = Math.pow(a, b);
//                if (!numbers.contains(power)) {
//                    numbers.add(power);
//                }
//            }
//        }
//        assert numbers.size() == 15;

        for (int a = 2; a < A_LIMIT; a++) {
            for (int b = 2; b < B_LIMIT; b++) {
                double power = Math.pow(a, b);
                if (!numbers.contains(power)) {
                    numbers.add(power);
                }
            }
        }
        System.out.println(numbers.size());
    }
}
