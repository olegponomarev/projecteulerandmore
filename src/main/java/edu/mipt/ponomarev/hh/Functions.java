package edu.mipt.ponomarev.hh;

import edu.mipt.ponomarev.euler.util.Utilities;

import java.util.List;

/**
 * Created by Oleg on 06.09.2016.
 */
public class Functions {
    private static List<Integer> primes = Utilities.getPrimes(12946);
    private static final int NUMBER = 12946;
    private static final int[][] P = new int[NUMBER + 1][NUMBER + 1];

    static {
        for (int i = 0; i < P.length; i++) {
            for (int j = 0; j < P[i].length; j++) {
                P[i][j] = -1;
            }
        }
    }

    /**
     * creepy function
     *
     * @param n some number
     * @param k - number of primes
     * @return 1 if there is a sum of k primes that equal to n, 0 if there is not
     */
    public static int getP(int n, int k) {
        if (P[n][k] != -1) {
            return P[n][k];
        }
        if (k == 1) {
            if (primes.contains(n)) {
                P[n][k] = 1;
                return 1;
            } else {
                P[n][k] = 0;
                return 0;
            }
        }
        if (n / 2 < k) {
            P[n][k] = 0;
            return 0;
        }
        /*if (n % k == 0 && primes.contains(n / k)) {
            P[n][k] = 1;
            return 1;
        }*/
        for (int prime : primes) {
            if (prime > n) {
                P[n][k] = 0;
                return 0;
            }
            if (P[n - prime][k - 1] == 1) {
                P[n][k] = 1;
                return 1;
            }
        }
        P[n][k] = 0;
        return 0;
    }

    public static void main(String[] args) {
        int[] array = new int[NUMBER + 1];
        array[1] = getP(1, 1);
        for (int n = 2; n < NUMBER + 1; n++) {
            array[n] += array[n - 1];
            for (int k = 1; k <= n / 2; k++) {
                array[n] += getP(n, k);
            }
//            if (n % 1000 == 0) {
//                System.out.println("S(" + n + ") = " + array[n]);
//            }
        }
        assert array[2] == 1;
        assert array[10] == 20;
        assert array[1000] == 248838;
        System.out.println("S(" + NUMBER + ") = " + array[NUMBER]);
    }
}
