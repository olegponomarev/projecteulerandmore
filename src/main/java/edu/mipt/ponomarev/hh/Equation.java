package edu.mipt.ponomarev.hh;

/**
 * Created by Oleg on 05.09.2016.
 */
public class Equation {
    public static final String eq = "rxxv + uus = ssrx";
    public static final String eq2 = "vww + vvw = rrss";
    static char[] chars = {'r', 's', 'u', 'v', 'x'};
    public static boolean equationIsCorrect(int r, int s, int u, int v, int x) {
        if (r == s || r == u || r == v || r == x || s == u || s == v || s == x || u == v || u == x || v == x) {
            return false;
        }
        String first = "" + r + x + x + v;
        String second = "" + u + u + s;
        String sum = "" + s + s + r + x;
        if (Integer.parseInt(first) + Integer.parseInt(second) == Integer.parseInt(sum)) {
            System.out.println(first + " + " + second + " = " + sum);
            return true;
        } else {
            return false;
        }
    }

    public static boolean equation2isCorrect(int r, int s, int v, int w) {
        /*if (r == s || r == v || r == w || s == v || s == w || v == w) {
            return false;
        }*/
        String first = "" + v + w + w;
        String second = "" + v + v + w;
        String sum = "" + r + r + s + s;
        if (Integer.parseInt(first) + Integer.parseInt(second) == Integer.parseInt(sum)) {
            System.out.println(first + " + " + second + " = " + sum);
            return true;
        } else {
            return false;
        }
    }

    public static void main(String[] args) {
        int count = 0;
        for (int a = 1; a < 10; a++) {
            for (int b = 1; b < 10; b++) {
                for (int c = 1; c < 10; c++) {
                    for (int d = 0; d < 10; d++) {
                        /*for (int e = 0; e < 10; e++) {
                            if (equationIsCorrect(a, b, c, d, e)) {
                                count++;
                            }
                        }*/
                        if (equation2isCorrect(a, b, c, d)) {
                            count++;
                        }

                    }

                }

            }
        }
        System.out.println(count);
    }
}
