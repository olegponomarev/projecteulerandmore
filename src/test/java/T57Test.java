import edu.mipt.ponomarev.euler.T57_SquareRootConvergents;
import org.junit.Assert;
import org.junit.Test;

/**
 * @version 1.0
 * @Author Oleg Ponomarev
 * @since 20.11.2016
 */
public class T57Test {
    @Test
    public void testFraction() {
        T57_SquareRootConvergents convergents = new T57_SquareRootConvergents( 1001 );
        Assert.assertEquals( "1/1", convergents.fractionToString( convergents.squareRoot( 0 ) ));
        Assert.assertEquals( "3/2", convergents.fractionToString( convergents.squareRoot( 1 ) ));
        Assert.assertEquals( "7/5", convergents.fractionToString( convergents.squareRoot( 2 ) ));
        Assert.assertEquals( "17/12", convergents.fractionToString( convergents.squareRoot( 3 ) ));
        Assert.assertEquals( "1393/985", convergents.fractionToString( convergents.squareRoot( 8 ) ));
    }
}
